/*global define */
define([], function () {
    'use strict';
	init();
	animate();

});

var SCREEN_WIDTH = window.innerWidth,
SCREEN_HEIGHT = window.innerHeight,
SCREEN_WIDTH_HALF = SCREEN_WIDTH  / 2,
SCREEN_HEIGHT_HALF = SCREEN_HEIGHT / 2;

var camera, scene, renderer,
birds, bird;

var boid, boids;

var stats;

var Bird = function() {	
	var basic = new THREE.MeshBasicMaterial( {color:0x333333} );
	var head = new THREE.Mesh(new THREE.SphereGeometry( .75, 10, 10 ), basic);
	var wingR = new THREE.Mesh(new THREE.CubeGeometry( 2, .1, 1 ), basic);
	var wingL = new THREE.Mesh(new THREE.CubeGeometry( 2, .1, 1 ), basic);
	this.jointR = new THREE.Mesh(new THREE.SphereGeometry( .25, 1, 1), basic);
	this.jointL = new THREE.Mesh(new THREE.SphereGeometry( .25, 1, 1), basic);
	this.jointR.position.x = -.5;
	this.jointL.position.x = .5;
	wingR.position.x = -1.;
	wingL.position.x = 1;
	this.jointL.add(wingL);
	this.jointR.add(wingR);
	this.object = new THREE.Object3D();
	this.object.add(head);
	this.object.add(this.jointL);
	this.object.add(this.jointR);
	this.object.scale.x=2;
	this.object.scale.y=2;
	this.object.scale.z=2;
	var flap = 0;
	var flapAmt = .1;
	this.fly = function(){
		if (flap >= .5) flapAmt = -.1;
		if (flap <= -.75) flapAmt = .1;
		flap += flapAmt;
		this.jointR.rotation.z += flapAmt;
		this.jointL.rotation.z -= flapAmt;	
	};
} 

var Boid = function() {

	var vector = new THREE.Vector3(),
	_acceleration, _width = 500, _height = 500, _depth = 200, _neighborhoodRadius = 100,
	_maxSpeed = 4, _maxSteerForce = 0.1;

	this.position = new THREE.Vector3();
	this.velocity = new THREE.Vector3();
	_acceleration = new THREE.Vector3();

	this.run = function ( boids ) {

		// Avoid screen bounds
		vector.set( - _width, this.position.y, this.position.z );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		vector.set( _width, this.position.y, this.position.z );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		vector.set( this.position.x, - _height, this.position.z );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		vector.set( this.position.x, _height, this.position.z );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		vector.set( this.position.x, this.position.y, - _depth );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		vector.set( this.position.x, this.position.y, _depth );
		vector = this.avoid( vector );
		vector.multiplyScalar( 5 );
		_acceleration.add( vector );

		
		if ( Math.random() > 0.5 ) {

			this.flock( boids );

		}

		this.move();

	}

	this.flock = function ( boids ) {

		_acceleration.add( this.alignment( boids ) );
		_acceleration.add( this.cohesion( boids ) );
		_acceleration.add( this.separation( boids ) );

	}

	this.move = function () {

		this.velocity.add( _acceleration );

		var l = this.velocity.length();

		if ( l > _maxSpeed ) {

			this.velocity.divideScalar( l / _maxSpeed );

		}

		this.position.add( this.velocity );
		_acceleration.set( 0, 0, 0 );

	}


	//

	this.avoid = function ( target ) {

		var steer = new THREE.Vector3();

		steer.copy( this.position );
		steer.sub( target );

		steer.multiplyScalar( 1 / this.position.distanceToSquared( target ) );

		return steer;

	}

	this.repulse = function ( target ) {

		var distance = this.position.distanceTo( target );

		if ( distance < 150 ) {

			var steer = new THREE.Vector3();

			steer.subVectors( this.position, target );
			steer.multiplyScalar( 0.5 / distance );

			_acceleration.add( steer );

		}

	}

// Rule 3: Boids try to match velocity with near boids.
	this.alignment = function ( boids ) {

		var boid, velSum = new THREE.Vector3(),
		count = 0;

		for ( var i = 0, il = boids.length; i < il; i++ ) {

			if ( Math.random() > 0.6 ) continue;

			boid = boids[ i ];

			distance = boid.position.distanceTo( this.position );

			if ( distance > 0 && distance <= _neighborhoodRadius ) {

				velSum.add( boid.velocity );
				count++;

			}

		}

		if ( count > 0 ) {

			velSum.divideScalar( count );

			var l = velSum.length();

			if ( l > _maxSteerForce ) {

				velSum.divideScalar( l / _maxSteerForce );

			}

		}

		return velSum;

	}

// Rule 3: Boids try to match velocity with near boids.

	this.cohesion = function ( boids ) {

		var boid, distance,
		posSum = new THREE.Vector3(),
		steer = new THREE.Vector3(),
		count = 0;

		for ( var i = 0, il = boids.length; i < il; i ++ ) {

			if ( Math.random() > 0.6 ) continue;

			boid = boids[ i ];
			distance = boid.position.distanceTo( this.position );

			if ( distance > 0 && distance <= _neighborhoodRadius ) {

				posSum.add( boid.position );
				count++;

			}

		}

		if ( count > 0 ) {

			posSum.divideScalar( count );

		}

		steer.subVectors( posSum, this.position );

		var l = steer.length();

		if ( l > _maxSteerForce ) {

			steer.divideScalar( l / _maxSteerForce );

		}

		return steer;

	}

// Rule 2: Boids try to keep a small distance away from other objects (including other boids).

	this.separation = function ( boids ) {

		var boid, distance,
		posSum = new THREE.Vector3(),
		repulse = new THREE.Vector3();

		for ( var i = 0, il = boids.length; i < il; i ++ ) {

			if ( Math.random() > 0.6 ) continue;

			boid = boids[ i ];
			distance = boid.position.distanceTo( this.position );

			if ( distance > 0 && distance <= _neighborhoodRadius ) {

				repulse.subVectors( this.position, boid.position );
				repulse.normalize();
				repulse.divideScalar( distance );
				posSum.add( repulse );

			}

		}

		return posSum;

	}

}
function init() {

				camera = new THREE.PerspectiveCamera( 75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
				camera.position.z = 350;

				scene = new THREE.Scene();
				scene.fog = new THREE.FogExp2( 0xffffff, 0.001 );

				birds = [];
				boids = [];

				for ( var i = 0; i < 200; i ++ ) {

					boid = boids[ i ] = new Boid();
					boid.position.x = Math.random() * 400 - 200;
					boid.position.y = Math.random() * 400 - 200;
					boid.position.z = Math.random() * 200 - 100;
					boid.velocity.x = Math.random() * 2 - 1;
					boid.velocity.y = Math.random() * 2 - 1;
					boid.velocity.z = Math.random() * 2 - 1;

					birds[ i ] = new Bird();
					bird = birds[i].object;
					bird.position = boids[ i ].position;
					scene.add( bird );


				}

				renderer = new THREE.WebGLRenderer();
				renderer.setClearColor(0xffffff, 1);
				renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

				document.body.appendChild( renderer.domElement );

				stats = new Stats();
				stats.domElement.style.position = 'absolute';
				stats.domElement.style.left = '0px';
				stats.domElement.style.top = '0px';

				document.getElementById( 'container' ).appendChild(stats.domElement);

				//

						}

			//

			function animate() {

				requestAnimationFrame( animate );

				render();
				stats.update();

			}

			function render() {

				for ( var i = 0, il = birds.length; i < il; i++ ) {

					boid = boids[ i ];
					boid.run( boids );

					bird = birds[ i ].object;

					bird.rotation.y = Math.atan2( - boid.velocity.z, boid.velocity.x );
					bird.rotation.z = Math.asin( boid.velocity.y / boid.velocity.length() );
					birds[i].fly();

				}

				renderer.render( scene, camera );

			}