$(document).ready(function(){
	init();
	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
});

var SCREEN_WIDTH = window.innerWidth,
SCREEN_HEIGHT = window.innerHeight,
SCREEN_WIDTH_HALF = SCREEN_WIDTH  / 2,
SCREEN_HEIGHT_HALF = SCREEN_HEIGHT / 2,
NEAR = 1, FAR = 10000, ANGLE = 60;
var camera, chaseCamera, scene, renderer,
birds, bird;

var mouseX = mouseY = 0;
var stats;
var scatter = false;
var _distanceFromOtherBirds = 30;
var _fogIntensity = .003;
var _rule1Mod = 1;
var _rule2Mod = .5;
var _rule3Mod = 1;
var _maxSpeed = 2;
var _maxAccel = .1;
var gui = new dat.GUI();
var trees = [];
var mouseControl = false;
var useChaseCamera = false;
controls = gui.addFolder("Controls");
separation_control = controls.add(this, "_distanceFromOtherBirds", 0, 200).name('Distance');
fog_control = controls.add(this, "_fogIntensity", .001, .009).name('Fog');
rule1Control = controls.add(this, "_rule1Mod", .1, 10).name('Attraction');
rule2Control = controls.add(this, "_rule2Mod", .1, 3).name('Avoidance');
rule3Control = controls.add(this, "_rule3Mod", .1, 10).name('Uniformity');
scatterControl = controls.add(this,"scatter").name('Scatter');
addTree = controls.add(this,"makeTree").name('Add Tree');
removeTrees = controls.add(this,"clearTrees").name('Clear Trees');
add25Birds = controls.add(this,"addBirds").name('Add 25 Birds');
mouseToggle = controls.add(this,"mouseControl").name("Mouse Tracking").listen();
useChase = controls.add(this,"useChaseCamera").name("Hawk-Cam").listen();
controls.open();

var barkTexture = new THREE.ImageUtils.loadTexture( 'images/bark.jpg' );
var barkMaterial = new THREE.MeshLambertMaterial( {map:barkTexture} );
barkTexture.wrapS = barkTexture.wrapT = THREE.RepeatWrapping; 
barkTexture.repeat.set( 1,5 );
var featherTexture = new THREE.ImageUtils.loadTexture( 'images/feathers.jpg' );
var hawkTexture = new THREE.ImageUtils.loadTexture( 'images/hawk.jpg' );
var grassTexture = new THREE.ImageUtils.loadTexture( 'images/grass.jpg' );
grassTexture.wrapS = grassTexture.wrapT = THREE.RepeatWrapping; 
grassTexture.repeat.set( 20,20 );
var hawkTexture = new THREE.ImageUtils.loadTexture('images/hawk.jpg');
var hawkFeather = new THREE.MeshPhongMaterial( { map: hawkTexture} );
var feather = new THREE.MeshPhongMaterial( { map: featherTexture} );
var beakMaterial = new THREE.MeshPhongMaterial( {color: 0xE8980E} );
var basic = new THREE.MeshBasicMaterial( { visible:false});
var eyeballMaterial = new THREE.MeshPhongMaterial( {color: 0xee5533} );
var materials = [];
materials.push(feather);
materials.push(eyeballMaterial);
materials.push(beakMaterial);
materials.push(hawkFeather);
var birdModel = initBirdModel();

window.onkeyup = function(e) {
   var key = e.keyCode ? e.keyCode : e.which;

   if (key == 32) {
       mouseControl = !mouseControl;
   }
} 

var Tree = function(){
	this.object = new THREE.Mesh(new THREE.CylinderGeometry( 10, 10, 500, 20, 20, false ), barkMaterial); 
	this.object.position.y = -50;
	this.object.castShadow = true;
	this.object.receiveShadow = true;
}

function clearTrees() {
	for(i=0;i<trees.length;i++){
		scene.remove(trees[i]);
	}
	trees = [];
}

function makeTree(){
	var tree = new Tree();
	tree.object.position.x = Math.random() * 400 - 200;
	tree.object.position.y = 200;
	tree.object.position.z = Math.random() * 300 - 150;
	scene.add(tree.object);
	trees.push(tree.object);
}

function onDocumentMouseMove( event ) {
	mouseX = event.clientX - SCREEN_WIDTH_HALF;
	mouseY = event.clientY - SCREEN_HEIGHT_HALF;
}


function init() {
	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2( 0x0096ff, _fogIntensity );

	camera = new THREE.PerspectiveCamera( ANGLE, SCREEN_WIDTH / SCREEN_HEIGHT, NEAR, FAR );
	camera.position.z = 200;
	chaseCamera = new THREE.PerspectiveCamera( ANGLE, SCREEN_WIDTH / SCREEN_HEIGHT, NEAR, FAR );
	scene.add(chaseCamera);


	var spotLight = new THREE.SpotLight( 0xffffff );
	spotLight.position.set( 100, 1000, 100 );
	spotLight.castShadow = true;
	spotLight.shadowMapWidth = 1024;
	spotLight.shadowMapHeight = 1024;
	spotLight.shadowCameraNear = 500;
	spotLight.shadowCameraFar = 4000;
	spotLight.shadowCameraFov = 30;
	scene.add( spotLight );
	var light = new THREE.AmbientLight( 0x404040 ); // soft white light
	scene.add( light );

	ground = new THREE.Mesh(new THREE.PlaneGeometry(3000, 3000, 5, 5), new THREE.MeshBasicMaterial({map:grassTexture}));
	ground.rotation.set(-90*Math.PI/180, 0, 0);
	ground.position.set(0, -50, 0);
	ground.receiveShadow = true;
	scene.add(ground);
	
	birds = [];
	hawk = new Hawk();

	scene.add(hawk.object);

	for ( var i = 0; i < 100; i ++ ) {
		birds[ i ] = bird = new Bird();
		bird.object.position.x = Math.random() * 400 - 200;
		bird.object.position.y = Math.random() * 250 - 45;
		bird.object.position.z = Math.random() * 200 - 100;
		bird.velocity.x = Math.random() * 20 - 10;
		bird.velocity.y = Math.random() * 20 - 10;
		bird.velocity.z = Math.random() * 20 - 10;

		bird.id = bird.object.name = i;
		scene.add(bird.object);
	}
	hawk.findPrey();
	for (var i = 0;i<10;i++){
		makeTree();
	}

	renderer = new THREE.WebGLRenderer();
	renderer.setClearColor(0x0096ff, 1);
	renderer.shadowMapEnabled = true;
	renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

	document.body.appendChild( renderer.domElement );

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.left = '0px';
	stats.domElement.style.top = '0px';

	document.getElementById( 'container' ).appendChild(stats.domElement);
	animate();
				//

}

			//

function animate() {

	requestAnimationFrame( animate );
	render();
	stats.update();

}

function addBirds(){
	j = birds.length + 25;
	for ( var i = birds.length; i < j; i ++ ) {
		birds[ i ] = bird = new Bird();
		bird.object.position.x = Math.random() * 400 - 200;
		bird.object.position.y = Math.random() * 250 - 45;
		bird.object.position.z = Math.random() * 200 - 100;
		bird.velocity.x = Math.random() * 20 - 10;
		bird.velocity.y = Math.random() * 20 - 10;
		bird.velocity.z = Math.random() * 20 - 10;

		bird.id = bird.object.name = i;
		scene.add(bird.object);
	}
}

function render() {
	for ( var i = 0; i < birds.length; i++ ) {
		bird = birds[ i ];
		bird.fly();
		bird.acceleration.set(0,0,0);
	}
	hawk.fly();
	hawk.acceleration.set(0,0,0);

		var relativeCameraOffset = new THREE.Vector3(-10,5,5);
		var cameraOffset = relativeCameraOffset.applyMatrix4( hawk.object.matrixWorld );
		chaseCamera.position.x = cameraOffset.x;
		chaseCamera.position.y = cameraOffset.y;
		chaseCamera.position.z = cameraOffset.z;
		chaseCamera.lookAt( hawk.object.position );

	scene.fog.density = _fogIntensity;
	if(mouseControl){
		camera.position.x += ( mouseX - camera.position.x ) * 0.01;
		if(camera.position.x>=199)camera.position.x = 199;
		else if(camera.position.x<=-199)camera.position.x = -199;
		camera.position.y += ( - mouseY - camera.position.y ) * 0.01;
		if(camera.position.y>=300)camera.position.x = 300;
		else if(camera.position.y<=0)camera.position.y = 0;
		camera.lookAt( scene.position );
	}
	if(useChaseCamera)renderer.render(scene,chaseCamera);
	else renderer.render( scene, camera );

}

