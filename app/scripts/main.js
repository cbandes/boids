require.config({
    paths: {
        jquery: '../bower_components/jquery/jquery',
        three: '../bower_components/three.js/three',
        stats: '../bower_components/stats.js/build/stats.min',
        gui : 'dat.gui.min'
    },
    shim: {
		"three": { deps: ["jquery"], exports: 'jquery' },
		"stats": {deps:["three"], exports:"three"},
		"app": {deps:["stats"], exports:"stats"}
	}
});

require(['jquery', 'three', 'stats','app','gui'], function (app, $) {
    'use strict';
});
