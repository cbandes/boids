function initBirdModel(type){
	var body = new THREE.Mesh(new THREE.SphereGeometry( .75, 10, 10 ), feather);
	var beak = new THREE.Mesh(new THREE.CylinderGeometry( 0, .25, 1, 5, 5, false ), beakMaterial);
	var tail = new THREE.Mesh(new THREE.CylinderGeometry( 0, .5, 2, 5, 5, false ), feather);
	if(type=="hawk"){
		var wingR = new THREE.Mesh(new THREE.CubeGeometry( 1, .1, 2 ), hawkFeather);
		var wingL = new THREE.Mesh(new THREE.CubeGeometry( 1, .1, 2 ), hawkFeather);
	}
	else {
		var wingR = new THREE.Mesh(new THREE.CubeGeometry( 1, .1, 2 ), feather);
		var wingL = new THREE.Mesh(new THREE.CubeGeometry( 1, .1, 2 ), feather);
	}
	var eyeR = new THREE.Mesh(new THREE.SphereGeometry( .1, 5, 5), eyeballMaterial);
	var eyeL = new THREE.Mesh(new THREE.SphereGeometry( .1, 5, 5), eyeballMaterial);
	if(type=="hawk") featherIndex = 3;
	else featherIndex = 0;
	$(body.geometry.faces).each(function(){this.materialIndex = featherIndex});
	$(tail.geometry.faces).each(function(){this.materialIndex = featherIndex});
	$(beak.geometry.faces).each(function(){this.materialIndex = 2});
	$(eyeR.geometry.faces).each(function(){this.materialIndex = 1});
	$(eyeL.geometry.faces).each(function(){this.materialIndex = 1});
	jointR = new THREE.Mesh(new THREE.SphereGeometry( .25, 3, 3), basic);
	jointL = new THREE.Mesh(new THREE.SphereGeometry( .25, 3, 3), basic);
	jointR.position.z = -.5;
	jointL.position.z = .5;
	beak.rotation.x= Math.PI/2;
	tail.rotation.x= Math.PI/2;
	beak.position.z = .9;
	tail.position.z = -.7;
	eyeR.position.z = .6;
	eyeL.position.z = .6;
	eyeR.position.y = .4;
	eyeL.position.y = .4;
	eyeR.position.x = -.25;
	eyeL.position.x = .25;
	wingR.position.z = -1;
	wingL.position.z = 1;
	var mergedBody = new THREE.Geometry();
	THREE.GeometryUtils.merge(mergedBody,body);
	THREE.GeometryUtils.merge(mergedBody,beak);
	THREE.GeometryUtils.merge(mergedBody,tail);
	THREE.GeometryUtils.merge(mergedBody,eyeR);
	THREE.GeometryUtils.merge(mergedBody,eyeL);
	var wholeBody = new THREE.Mesh(mergedBody, new THREE.MeshFaceMaterial( materials ));
	beak = null;
	tail = null;
	eyeR = null;
	eyeL = null;
	body = null;
	wholeBody.rotation.y = Math.PI/2;	
	wholeBody.castShadow = true;
	wingR.castShadow = true;
	wingL.castShadow = true;
	jointL.add(wingL);
	jointR.add(wingR);
	jointL.name = "jointL";
	jointR.name = "jointR";
	var _birdModel = new THREE.Object3D();
	_birdModel.add(wholeBody);
	_birdModel.add(jointL);
	_birdModel.add(jointR);
	var scaleFactor = 3;
	if(type=="hawk") scaleFactor = 10;
	_birdModel.scale.x=scaleFactor;
	_birdModel.scale.y=scaleFactor;
	_birdModel.scale.z=scaleFactor;
	_birdModel.frustrumCulled = true;
	return _birdModel;
}

// Bird object
var Bird = function() {	
	this.object = birdModel.clone();
	this.jointR = this.object.children[2];
	this.jointL = this.object.children[1];
	this.chased = false;
	this.velocity = new THREE.Vector3();
	this.acceleration = new THREE.Vector3(0,0,0);
	var flap = 0;
	var flapAmt = .25;
	
	// Animation
	this.fly = function(){
		this.applyRule1();
		this.applyRule2();
		this.applyRule3();
		this.avoidTrees();
		this.avoidHawk();
		this.velocity.add(this.acceleration);
		this.stayOnScreen();			
		this.lookForward();
		//Velocity limit courtesy mrdoob
		var l = this.velocity.length();
		if ( l > _maxSpeed ) {
			this.velocity.divideScalar( l / _maxSpeed );
		}
		if(hawk.prey == this)this.velocity.multiplyScalar(2);
		this.object.position.add(this.velocity);
		if (flap >= .5) flapAmt = -.1;
		if (flap <= -.75) flapAmt = .1;
		flap += flapAmt;
		this.jointR.rotation.x += flapAmt;
		this.jointL.rotation.x -= flapAmt;	

	};

	// Boid Rules 1-3 - inspired by the pseudocode and other notes at kfish.org

	// Rule 1: Boids try to fly towards the centre of mass of neighbouring boids.
	this.applyRule1 = function(){
		var toCenter = new THREE.Vector3();
		var n = 0;
		for (var i = 0; i < birds.length; i++) {
			distance = birds[i].object.position.distanceTo( this.object.position );
			if ((distance>0)&&(distance<_distanceFromOtherBirds)){
				toCenter.add(birds[i].object.position);
				n++;				
			}
		}
		if(toCenter.equals(new THREE.Vector3(0, 0, 0))) 
			{
				this.acceleration.add(this.velocity.normalize());
			}
		if(n>0)toCenter.divideScalar(n);
		toCenter.sub(this.object.position);
		var l = toCenter.length();

		if ( l > _maxAccel ) {

			toCenter.divideScalar( l / _maxAccel );

		}

		if(scatter) toCenter.negate();
		if(this.chased) toCenter.negate();
		this.acceleration.add(toCenter.multiplyScalar(_rule1Mod));
	}

	//Rule 2: Boids try to keep a small distance away from other objects (including other boids).
	this.applyRule2 = function(){
		var keepAway = new THREE.Vector3();
		for (var i = 0; i < birds.length; i++) {
			distance = birds[i].object.position.distanceTo( this.object.position );
			if ((distance>0)&&(distance<_distanceFromOtherBirds*_rule2Mod)){
				var p = new THREE.Vector3();
				p.subVectors(this.object.position, birds[i].object.position);
				p.normalize();
				p.divideScalar(distance);
				keepAway = keepAway.add(p);
			}
		}
		this.acceleration.add(keepAway);
	}

	// Rule 3: Boids try to match velocity with near boids.
	this.applyRule3 = function(){
		var c = new THREE.Vector3();
		var n = 0;
		for (var i = 0; i < birds.length; i++) {
			distance = birds[i].object.position.distanceTo( this.object.position );
			if ((distance>0)&&(distance<_distanceFromOtherBirds)){
				c.add(birds[i].velocity);
				n++;					
			}
		}
		if(n>0)c.divideScalar(n);
		c.sub(this.velocity);
		this.acceleration.add(c.divideScalar(100/_rule3Mod));
	}

	this.stayOnScreen = function(){
		if(this.object.position.x<-275) this.velocity.x = 2;
		else if (this.object.position.x>275) this.velocity.x = -2;
		if(this.object.position.y<20) this.velocity.y+=.05;
		if(this.object.position.y<-48) this.velocity.y =  1;
		if (this.object.position.y>215) this.velocity.y -=.01;
		if (this.object.position.y>275) this.velocity.y =  -2;
		if(this.object.position.z<-(100)) this.velocity.z = 1;
		else if (this.object.position.z>(250)) this.velocity.z = -2;
	}

	this.lookForward = function(){
		this.object.rotation.y = Math.atan2( - this.velocity.z, this.velocity.x );
		this.object.rotation.z = Math.asin( this.velocity.y / this.velocity.length() );
	}

	this.avoidTrees = function(){
		var keepAway = new THREE.Vector3(0,0,0);
		var xzBird = new THREE.Vector2( this.object.position.x, this.object.position.z );
		for (var i = 0; i < trees.length; i++) {
			xzTree = new THREE.Vector2( trees[i].position.x, trees[i].position.z );
			distance = xzTree.distanceTo( xzBird );	
			if ((distance>0)&&(distance<25)){
				var xzP = new THREE.Vector2(0,0,0);
				xzP.subVectors(xzBird, xzTree);
				xzP.divideScalar(distance);
				xzP.normalize();
				var p = new THREE.Vector3( xzP.x, 0, xzP.y );
				keepAway = keepAway.add(p);
			}
		}
		this.acceleration.add(keepAway);
	}

	this.avoidHawk = function(){
		var keepAway = new THREE.Vector3(0,0,0);
		distance = hawk.object.position.distanceTo( this.object.position );
		if ((distance>0)&&(distance<50)){
			this.chased = true;
			var p = new THREE.Vector3();
			p.subVectors(this.object.position, hawk.object.position);
			p.normalize();
			//p.divideScalar(distance);
			keepAway = keepAway.add(p);
		}
		else this.chased = false;
		this.acceleration.add(keepAway);
	}
} 

