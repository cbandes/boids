// Hawk object - I wish this were an extension of the Bird class, but I haven't had much success with that...

var Hawk = function(){
	this.object = initBirdModel("hawk");
	this.jointR = this.object.children[2];
	this.jointL = this.object.children[1];
	this.velocity = new THREE.Vector3();
	this.acceleration = new THREE.Vector3(0,0,0);
	var flap = 0;
	var flapAmt = .25;

	this.prey = null;
	this.preyID = 0;
	
	this.findPrey = function(){
		if(this.prey == null){
			this.prey = birds[0];
		}
		this.preyDistance = this.prey.object.position.distanceTo(this.object.position);
		for (var i = 1; i<birds.length;i++){
			distance = this.object.position.distanceTo(birds[i].object.position);
			if(distance<this.preyDistance){
				this.prey = birds[i];
				this.preyID = i;
			}
		}
	}

// Rule 1: Boids try to fly towards the centre of mass of neighbouring boids.
	this.applyRule1 = function(){
		var toPrey = new THREE.Vector3();
		toPrey.copy(this.prey.object.position);
		toPrey.sub(this.object.position);
		this.acceleration.add(toPrey.multiplyScalar(0.5));	}
	
	this.stayOnScreen = function(){
		if(this.object.position.x<-275) this.velocity.x = 2;
		else if (this.object.position.x>275) this.velocity.x = -2;
		if(this.object.position.y<20) this.velocity.y+=.1;
		if(this.object.position.y<-38) this.velocity.y =  2;
		if (this.object.position.y>215) this.velocity.y -=.01;
		if (this.object.position.y>275) this.velocity.y =  -2;
		if(this.object.position.z<-(100)) this.velocity.z = 1;
		else if (this.object.position.z>(250)) this.velocity.z = -2;
	}
	
	this.lookForward = function(){
		this.object.rotation.y = Math.atan2( - this.velocity.z, this.velocity.x );
		this.object.rotation.z = Math.asin( this.velocity.y / this.velocity.length() );
	}

	// UNUSED FUNCTION, FOR FUTURE EXPANSION
	this.eatPrey = function(){
		if(this.object.position.distanceTo(this.prey.object.position)<10)
		{
			scene.remove(birds[this.preyID]);
			birds.splice(this.preyID,1);
			this.findPrey(); 
		}
	}

	this.avoidTrees = function(){
		var keepAway = new THREE.Vector3(0,0,0);
		var xzBird = new THREE.Vector2( this.object.position.x, this.object.position.z );
		for (var i = 0; i < trees.length; i++) {
			xzTree = new THREE.Vector2( trees[i].position.x, trees[i].position.z );
			distance = xzTree.distanceTo( xzBird );	
			if ((distance>0)&&(distance<60)){
				var xzP = new THREE.Vector2(0,0,0);
				xzP.subVectors(xzBird, xzTree);
				xzP.divideScalar(distance);
				xzP.normalize();
				var p = new THREE.Vector3( xzP.x, 0, xzP.y );
				keepAway = keepAway.add(p);
			}
		}
		this.acceleration.add(keepAway);
	}

	// Animation
	this.fly = function(){
		rand = Math.random();
		if(rand>.85) this.findPrey();
		this.applyRule1();

		this.avoidTrees();
		this.stayOnScreen();			
		this.velocity.add(this.acceleration);

		this.lookForward();
		//Velocity limit courtesy mrdoob
		var l = this.velocity.length();
		if ( l > _maxSpeed ) {
			this.velocity.divideScalar( l / _maxSpeed );
		}
		this.object.position.add(this.velocity);
		if (flap >= .5) flapAmt = -.1;
		if (flap <= -.75) flapAmt = .1;
		flap += flapAmt;
		this.jointR.rotation.x += flapAmt;
		this.jointL.rotation.x -= flapAmt;	

	};

}

